import { Ingredient } from './shared/ingredient.model';
import { EventEmitter } from '@angular/core';

export class ShoppingListService {
  dataChanged = new EventEmitter<Ingredient[]>();

  private ingredients: Ingredient[] = [
    new Ingredient('Apples', 5),
    new Ingredient('tomato', 2),
  ];

  getIngredients() {
    return this.ingredients.slice();
  }

  addIngredient(ingredient: Ingredient) {
    this.ingredients.push(ingredient);
    this.dataChanged.emit(this.ingredients.slice());
  }

  addRecipeIngredients(ingredients: Ingredient[]) {
    this.ingredients.push(...ingredients);
    this.dataChanged.emit(this.ingredients.slice());
  }
}
