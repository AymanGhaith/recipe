import { Recipe } from './recipe.model';
import { EventEmitter, Injectable } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list.service';

@Injectable()
export class RecipeService {
  recipeSelected = new EventEmitter<Recipe>();

  private recipes: Recipe[] = [
    // tslint:disable-next-line:max-line-length
    new Recipe(
      'recipe1',
      'recipe1 descreption',
      'https://www.landolakes.com/lolretail/media/LOLR-Media/Recipe-Collections/easy-dinner-recipes-for-kids.jpg?ext=.jpg',
      [
        new Ingredient('aaaa', 4),
        new Ingredient('second', 5)
      ]),
    new Recipe(
      'recipe2',
      'recipe2 descreption',
      // tslint:disable-next-line:max-line-length
      'https://res.cloudinary.com/hellofresh/image/upload/f_auto,fl_lossy,h_436,q_auto/v1/hellofresh_s3/image/5abd494fae08b54b610ca122-8bfc3c25.jpg',
      [
        new Ingredient('aaaa', 4),
        new Ingredient('second', 5)
      ]),
    // tslint:disable-next-line:max-line-length
    new Recipe(
      'recipe3',
      'recipe3 descreption',
      'https://www.landolakes.com/lolretail/media/LOLR-Media/Recipe-Collections/easy-dinner-recipes-for-kids.jpg?ext=.jpg',
      [
        new Ingredient('aaaa', 4),
        new Ingredient('second', 5)
      ])
  ];

  constructor(private shoppingListService: ShoppingListService) {}

  getRecipes() {
    return this.recipes.slice();
  }

  sendIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.shoppingListService.addRecipeIngredients(ingredients);
  }
}
